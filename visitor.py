import math
import re
from config import *
from custom_parser.SecondOrderDatalogLexer import SecondOrderDatalogLexer
from custom_parser.SecondOrderDatalogParser import SecondOrderDatalogParser
from custom_parser.SecondOrderDatalogVisitor import SecondOrderDatalogVisitor


# Visitor that is used to fetch information about relations apearing in program
class RelationsVisitor(SecondOrderDatalogVisitor):
  
    # Visit a parse tree produced by SecondOrderDatalogParser#fact.
    def visitFact(self, ctx:SecondOrderDatalogParser.FactContext):

        relation_name = ctx.children[0].getText()
        fact = ctx.getText()
        facts.append(fact)
        fact_args = fact[fact.find("(")+len("("):fact.rfind(")")].split(',')
        for arg in fact_args:
            constants.add(arg)
        arity = len(fact[fact.find("(")+len("("):fact.rfind(")")].split(','))
        if relation_name not in relations:
            relations[relation_name] = {}
            relations[relation_name]["arity"] = arity
            relations[relation_name]["tuples"] = {}
            relations[relation_name]["tuples"]["positives"] = []
            relations[relation_name]["tuples"]["negatives"] = []
            relations[relation_name]["scheme"] = ['i' for _ in range(arity)]
        return self.visitChildren(ctx)
    

    def visitGoal(self, ctx:SecondOrderDatalogParser.GoalContext):
        def is_constant(str):
            return (str[0].islower() and "(" not in str)

        goal = ctx.getText()
        goal_args = goal[goal.find("(")+len("("):goal.rfind(")")].split(',')
        for arg in goal_args:
            if is_constant(arg) and arg not in relations.keys():
                constants.add(arg)
        relation_name = goal.split("(")[0]
        arity = len(goal[goal.find("(")+len("("):goal.rfind(")")].split(','))
        
        relations[relation_name] = {}
        relations[relation_name]["type"] = "idb"
        relations[relation_name]["arity"] = arity
        relations[relation_name]["tuples"] = {}
        relations[relation_name]["tuples"]["positives"] = []
        relations[relation_name]["tuples"]["negatives"] = []

        return self.visitChildren(ctx)


    def visitBody(self, ctx:SecondOrderDatalogParser.BodyContext):
        def is_constant(str):
            return (str[0].islower() and "(" not in str)
        body = ctx.getText()
        body_rules = re.split(r',\s*(?![^()]*\))', body)
        for rule in body_rules:
            relation_name = rule.split("(")[0]
            arity = len(rule[rule.find("(")+len("("):rule.rfind(")")].split(','))
            if relation_name not in relations and is_constant(relation_name):
                relations[relation_name] = {}
                relations[relation_name]["arity"] = arity
                relations[relation_name]["tuples"] = {}
                relations[relation_name]["tuples"]["positives"] = []
                relations[relation_name]["tuples"]["negatives"] = []

        return self.visitChildren(ctx)

    def visitAtom(self, ctx:SecondOrderDatalogParser.AtomContext):
        def is_constant(str):
            return (str[0].islower() and "(" not in str)
        atom = ctx.parentCtx.getText()
        if atom[0] == "!":
            atom = atom[1:]
        atom_args = atom[atom.find("(")+len("("):atom.rfind(")")].split(',')
        for arg in atom_args:
            if is_constant(arg) and arg not in relations.keys():
                constants.add(arg)
        return self.visitChildren(ctx)
    
    # Visit a parse tree produced by SecondOrderDatalogParser#r.
    def visitR(self, ctx:SecondOrderDatalogParser.RContext):
        if self.is_rule(ctx.getText()):
            new_rule = ctx.getText()
            rules.append(self.parse_rule(new_rule))

        return self.visitChildren(ctx)

    def is_rule(self,str):
        return (":-" in str) 

    # separate head from body and return them as dict
    def parse_rule(self,rule):
        head = rule.split(":-")[0]
        body = rule.split(":-")[1].replace('.','')
        
        body_rules = re.split(r',\s*(?![^()]*\))', body)
        return {"head":head, "body":body_rules}