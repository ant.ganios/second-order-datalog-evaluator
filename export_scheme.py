from config import *

def is_variable(str):
    return str[0].isupper()

def is_constant(str):
    return (str[0].islower() and "(" not in str and type(str) is not list)


#Given the rules of the program, we export the scheme of the corresponding relations
def export_scheme_from_rules():
    for rule in rules:
        head = rule["head"]
        head_name = head.split('(')[0]
        head_arguments = head[head.find("(")+len("("):head.rfind(")")].split(',')
        head_variables = list(filter(lambda arg: is_variable(arg),head_arguments))

        # create a dictionary that contains information for every variable that appears in the head of our rule
        head_variables_scheme = {}
        for var in head_variables:
            head_variables_scheme[var] = ""

        subgoals = rule["body"]
        for subgoal in subgoals:
            predicate_name = subgoal.split('(')[0]
            subgoal_arguments = subgoal[subgoal.find("(")+len("("):subgoal.rfind(")")].split(',')
            subgoal_variables = list(filter(lambda arg: is_variable(arg),subgoal_arguments))

            # if we have a predicate variable, every argument must be data object (we examine second order)
            if predicate_name in head_variables:
                head_variables_scheme[predicate_name] =  ['i' for _ in range(len(subgoal_arguments))] 
                for var in subgoal_variables:
                    if var in head_variables:
                        head_variables_scheme[var] = "i"
            else:
                for var in subgoal_variables:
                    if var in head_variables:
                        if var in relations.keys():
                            head_variables_scheme[var] = relations[var]["scheme"]
                        else:
                            head_variables_scheme[var] = "i"

        relation_scheme = []
        for arg in head_arguments:
            if arg in head_variables:
                relation_scheme.append(head_variables_scheme[arg])
            else:
                relation_scheme.append('i')
        relations[head_name]["scheme"] = relation_scheme


