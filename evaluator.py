from operator import sub
from preprocess import preprocess_input_file
from config import *
import pprint
import itertools
import sys

class SecondOrderEvaluator():

    
    def is_variable(self,str):
        return str[0].isupper()

    def is_constant(self,str):
        return (str[0].islower() and "(" not in str and type(str) is not list)


    """
        Given the rules of the program, we export the scheme of the corresponding relations
    """
    def export_scheme_from_rules(self):
        for rule in rules:
            head = rule["head"]
            head_name = head.split('(')[0]
            head_arguments = head[head.find("(")+len("("):head.rfind(")")].split(',')
            head_variables = list(filter(lambda arg: self.is_variable(arg),head_arguments))

            head_variables_scheme = {}
            for var in head_variables:
                head_variables_scheme[var] = ""

            subgoals = rule["body"]
            for subgoal in subgoals:
                predicate_name = subgoal.split('(')[0]
                subgoal_arguments = subgoal[subgoal.find("(")+len("("):subgoal.rfind(")")].split(',')
                subgoal_variables = list(filter(lambda arg: self.is_variable(arg),subgoal_arguments))

                if predicate_name in head_variables:
                    head_variables_scheme[var] = "r"
                    for var in subgoal_variables:
                        if var in head_variables:
                            head_variables_scheme[var] = "i"
                else:
                    for var in subgoal_variables:
                        if var in head_variables:
                            if var in relations.keys():
                                head_variables_scheme[var] = "r"
                            else:
                                head_variables_scheme[var] = "i"

            relation_scheme = []
            for arg in head_arguments:
                if arg in head_variables:
                    relation_scheme.append(head_variables_scheme[arg])
                else:
                    relation_scheme.append('i')
            relations[head_name]["scheme"] = relation_scheme
            relations[head_name]["arity"] = len(relation_scheme)

    """
    Given a subgoal, returns the scheme of the arguments
    """
    def extract_types_dict(self,subgoal):
        types_dict = {}
        predicate_name = subgoal.split('(')[0]
        predicate_arguments = subgoal[subgoal.find("(")+len("("):subgoal.rfind(")")].split(',')
        
        if self.is_variable(predicate_name):
            types_dict[predicate_name] = 'r'
        for i in range(0,len(predicate_arguments)):
            types_dict[predicate_arguments[i]] = 'r' if type(relations[predicate_name]["scheme"][i]) is list else 'i'
        return types_dict




    def match(self,a,b,types_dict,substitution):
        if type(a) is list:
            if type(b) is list and len(a) == len(b):
                for i in range(0,len(a)):
                    valid_match,_ = self.match(a[i],b[i],types_dict,substitution)
                    if valid_match == False:
                        return False,{}
                return True,substitution
            return False,substitution

        if self.is_variable(a): # if a is variable
            if a not in substitution.keys(): # if a is undefined:
                substitution[a] = b
                return True,substitution
            if types_dict[a] == 'i': # if a is data object
                if substitution[a] == b:
                    return True,substitution
                return False,{}
            else: # if a is predicate
                substitution[a].append(b)
                return True,substitution
            

        if self.is_constant(a): # if a is constant
            if a not in relations.keys(): # if a is data object
                if a == b:
                    return True,substitution
                return False,substitution
            else: # if a is predicate
                a_positive_tuples = relations[a]["tuples"]["positives"]
                a_negative_tuples = relations[a]["tuples"]["negatives"]

                for b_tuple in b["positives"]: 
                    for a_tuple in a_positive_tuples:
                        valid_match,_ = self.match(a_tuple,b_tuple,types_dict,substitution)
                        if valid_match == True:
                            break
                    if valid_match == False:
                        return False,substitution

                for b_tuple in b["negatives"]: 
                    for a_tuple in a_negative_tuples:
                        valid_match,_ = self.match(a_tuple,b_tuple,types_dict,substitution)
                        if valid_match == True:
                            break
                    if valid_match == False:
                        return False,substitution
                return True,substitution
     
               




    # returns a relation with attributes corresponding to the variables in a predicate's arguments.
    def atov(self,subgoal):
        predicate_name = subgoal.split('(')[0]
        predicate_arguments = subgoal[subgoal.find("(")+len("("):subgoal.rfind(")")].split(',')
        predicate_variables = list(filter(lambda arg: self.is_variable(arg),predicate_arguments))

        output_relation = {}
        for var in predicate_variables:
            output_relation[var] = []
        for var in predicate_variables:
            output_relation[var] = []
        if predicate_name not in relations.keys() and self.is_constant(predicate_name):
            return output_relation
        if predicate_name[0] != "!": # without negation
            if self.is_constant(predicate_name):
                types_dict = self.extract_types_dict(subgoal)
                for t in relations[predicate_name]["tuples"]["positives"]:
                    a = predicate_arguments
                    valid_match,substitution = self.match(a,t,types_dict,{})
                    if valid_match:
                        new_tuple = []
                        for var in predicate_variables:
                            output_relation[var].append(substitution[var])
                            # new_tuple.append(substitution[var])
            else: # predicate variable
            
                # compute cartesian product of variables
                variable_tuples = [constants for _ in predicate_variables]
                for element in itertools.product(*variable_tuples):
                    new_tuple = list(element)
                    for i in range(0,len(predicate_variables)):
                        output_relation[predicate_variables[i]].append(new_tuple[i])          

                new_tuple = predicate_arguments
                output_relation_size = pow(len(constants),len(predicate_variables))
                output_relation[predicate_name] = []
                for i in range(0,output_relation_size):
                    new_tuple = []
                    for arg in predicate_arguments:
                        if arg in predicate_variables:
                            new_tuple.append(output_relation[arg][i])
                        else:
                            new_tuple.append(arg)
                    output_relation[predicate_name].append({"positives": [new_tuple],"negatives": [[]]})
                  
        else:
            if self.is_constant(predicate_name):
                types_dict = self.extract_types_dict(subgoal)
                for t in relations[predicate_name]["tuples"]["negatives"]:
                    a = predicate_arguments
                    valid_match,substitution = self.match(a,t,types_dict,{})
                    if valid_match:
                        new_tuple = []
                        for var in predicate_variables:
                            output_relation[var].append(substitution[var])
                            new_tuple.append(substitution[var])
            else: # predicate variable
            
                # compute cartesian product of variables
                variable_tuples = [constants for _ in predicate_variables]
                for element in itertools.product(*variable_tuples):
                    new_tuple = list(element)
                    for i in range(0,len(predicate_variables)):
                        output_relation[predicate_variables[i]].append(new_tuple[i])          

                new_tuple = predicate_arguments
                output_relation_size = pow(len(constants),len(predicate_variables))
                output_relation[predicate_name] = []
                for i in range(0,output_relation_size):
                    new_tuple = []
                    for arg in predicate_arguments:
                        if arg in predicate_variables:
                            new_tuple.append(output_relation[arg][i])
                        else:
                            new_tuple.append(arg)
                    
                    output_relation[predicate_name].append({"positives": [[]],"negatives": [new_tuple]})
             

    
        return output_relation
                


    def natural_join(self,relation1,relation2):
 
        relation1_attributes = list(relation1.keys())
        relation2_attributes = list(relation2.keys())
        
        relation1_values = list(relation1.values())
        relation2_values = list(relation2.values())
       
        relation1_size = len(relation1_values[0])
        relation2_size = len(relation2_values[0])
       

        relation1_tuples = []
        for k in range(0,relation1_size):
            new_tuple = []
            for i in range(0,len(relation1_attributes)):
                new_tuple.append(relation1_values[i][k])
            relation1_tuples.append(new_tuple)

        relation2_tuples = []
        for k in range(0,relation2_size):
            new_tuple = []
            for i in range(0,len(relation2_attributes)):
                new_tuple.append(relation2_values[i][k])
            relation2_tuples.append(new_tuple)

        relation1_columns = relation1_attributes
        relation2_columns = relation2_attributes
        relation1_map = {k: i for i, k in enumerate(relation1_attributes)}
        relation2_map = {k: i for i, k in enumerate(relation2_attributes)}

        join_on = [val for val in relation1_columns if val in relation2_columns]
        diff = [val for val in relation2_columns if val not in join_on]

        def match(row1, row2):
            return all(row1[relation1_map[rn]] == row2[relation2_map[rn]] for rn in join_on)

        body_variables_tuples  = []
        for r1_row in relation1_tuples:
            for r2_row in relation2_tuples:
                if match(r1_row, r2_row):
                    row = r1_row[:]
                    for rn in diff:
                        row.append(r2_row[relation2_map[rn]])
                    body_variables_tuples.append(row)

        body_variables_relation = {}
        new_attributes = []
        for attr1 in relation1_attributes:
            new_attributes.append(attr1)
        for attr2 in relation2_attributes:
            if attr2 not in new_attributes:
                new_attributes.append(attr2)

        for i in range(0,len(new_attributes)):
            new_column = []
            for k in range(0,len(body_variables_tuples)):
                new_column.append(body_variables_tuples[k][i])
            body_variables_relation[new_attributes[i]] = new_column 

        return body_variables_relation



    def rename_variables(self,subgoal_relations):
        preserved_variables = set([])

        for i in range(0,len(subgoal_relations)):
            for attr in list(subgoal_relations[i].keys()):
                if isinstance(subgoal_relations[i][attr][0], dict): # if we have a predicate variable
                    new_attr = attr +  "_" + str(i)
                    subgoal_relations[i].update({new_attr: subgoal_relations[i][attr]})
       
                    subgoal_relations[i].pop(attr)
                    preserved_variables.add(attr)

        return subgoal_relations,preserved_variables


    def preserve_relations(self,subgoals,preserved_variables):
        preserved_relations = {}
        for subgoal in subgoals:
            for var in preserved_variables:
                if var in subgoal.keys():
                    preserved_relations[var] = subgoal[var]
        return preserved_relations

    def remove_duplicates_from_relation(self,relation):
        new_relation = {}
        new_relation["positives"] = []
        new_relation["negatives"] = []
        for i in range(0,len(relation["positives"])):
            if relation["positives"][i] not in relation["positives"][i+1:]:
                new_relation["positives"].append(relation["positives"][i])
        for i in range(0,len(relation["negatives"])):
            if relation["negatives"][i] not in relation["negatives"][i+1:]:
                new_relation["negatives"].append(relation["negatives"][i])
        return new_relation

    def extended_join(self,subgoal_relations):
        subgoals,preserved_variables = self.rename_variables(subgoal_relations)

        relation_1 = subgoals[0]
        relation_2 = subgoals[1]
        body_variables_relation = self.natural_join(relation_1,relation_2)
        for i in range(2,len(subgoals)):
            relation_1 = body_variables_relation
            relation_2 = subgoals[i]
            body_variables_relation = self.natural_join(relation_1,relation_2)

        
        all_tuples = []
        body_variables_relation_size = len(list(body_variables_relation.values())[0])
        for i in range(0,body_variables_relation_size):
            new_tuple = {}
            for k in body_variables_relation.keys():
                new_tuple[k] = body_variables_relation[k][i]
            all_tuples.append(new_tuple)

        for var in preserved_variables: # for all variables Vπ
            attributes = []
            for attr in body_variables_relation.keys():
                if attr.startswith(var+"_"):
                    attributes.append(attr)
               

            body_variables_relation[var] = [] # add an attribute Vπ to Q

            for t in all_tuples:
                new_relation = {}
                combined_positives = []
                combined_negatives = []
                for k in t.keys():
                    if k in attributes:
                        if t[k]["positives"] not in combined_positives:
                            combined_positives = combined_positives + t[k]["positives"]
                        if t[k]["negatives"] not in combined_negatives:
                            combined_negatives = combined_negatives + t[k]["negatives"]
                new_relation["positives"] = combined_positives
                new_relation["negatives"] = combined_negatives
                body_variables_relation[var].append(self.remove_duplicates_from_relation(new_relation))
            
            for attr in attributes:
                body_variables_relation.pop(attr)        
 
        return body_variables_relation


    def vtoa(self,head,body_relation):
        head_name = head.split('(')[0]
        head_arguments = head[head.find("(")+len("("):head.rfind(")")].split(',')
        head_variables = list(filter(lambda arg: self.is_variable(arg),head_arguments))

        body_relation_size = len(list(body_relation.values())[0])
        head_relation_tuples = []
        for i in range(0,body_relation_size):
            new_tuple = []
            for arg in head_arguments:
                if arg in head_variables:
                    new_tuple.append(body_relation[arg][i])
                else:
                    new_tuple.append(arg)
            if new_tuple not in head_relation_tuples:
                head_relation_tuples.append(new_tuple)

        head_relation = {}
        head_relation["tuples"] = head_relation_tuples

        return head_name, head_relation