import os
import pprint
import itertools
from config import *

def preprocess_input_file(filename):
    tmp_filename = filename + "_sorted"
    os.system('grep -v \'^$\' ' + filename + ' | sort -b -k 2.2 > ' + tmp_filename )
    return tmp_filename



def extract_constants():
    for rule in rules:
        print(rule)


def extract_herbrand_universe():
    for r in relations.keys():
        if "type" in relations[r].keys():
            arity = relations[r]["arity"]
            if arity != 1:
                perms = list(itertools.permutations(constants, arity))
                for p in perms:
                    herbrand_universe.add(r + "("+ ",".join(p) +")")
            else:
                for c in constants:
                    herbrand_universe.add(r + "("+ c +")")
                
    return herbrand_universe
