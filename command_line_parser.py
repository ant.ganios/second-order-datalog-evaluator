import argparse

def parse_command_line():
    parser = argparse.ArgumentParser(description='Second order + negavtion datalog evaluator!')
    parser.add_argument('--input', required=True, type=str, help='Input file that will be evaluated')
    parser.add_argument('-o', '--output', help='Where the final relations will be stored')

    args = parser.parse_args()

    input_file = args.input
    output_file = args.output if args.output != None else "stdout"
    
    return input_file,output_file