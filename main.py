import sys
import pprint
from command_line_parser import parse_command_line
from preprocess import extract_constants, preprocess_input_file,extract_herbrand_universe
from antlr4 import *
from custom_parser.SecondOrderDatalogLexer import SecondOrderDatalogLexer
from custom_parser.SecondOrderDatalogParser import SecondOrderDatalogParser
from custom_parser.SecondOrderDatalogVisitor import SecondOrderDatalogVisitor
from visitor import RelationsVisitor
from config import *
from export_scheme import *
from evaluator import SecondOrderEvaluator


input_filename,output_filename = parse_command_line()

input_file = FileStream(input_filename)
lexer = SecondOrderDatalogLexer(input_file)
stream = CommonTokenStream(lexer)
parser = SecondOrderDatalogParser(stream)
tree = parser.program()

visitor = RelationsVisitor()
output = visitor.visit(tree)

export_scheme_from_rules()

evaluator = SecondOrderEvaluator()

for fact in facts:
    relation_name = fact.split('(')[0]
    fact_arguments = fact[fact.find("(")+len("("):fact.rfind(")")].split(',')
    if fact_arguments not in relations[relation_name]["tuples"]["positives"]:
        relations[relation_name]["tuples"]["positives"].append(fact_arguments)



while 1:
    for rule in rules:
        subgoal_relations = []
        for subgoal in rule["body"]:
            new_relation = evaluator.atov(subgoal)
            subgoal_relations.append(new_relation)

        if len(subgoal_relations) == 1:
            body_variables_relation = subgoal_relations[0]
        else:
            body_variables_relation = evaluator.extended_join(subgoal_relations)
        
        head_name,head_relation = evaluator.vtoa(rule["head"],body_variables_relation)
        relation = relations[head_name]
        new_tuples_added = False
        for t in head_relation['tuples']:
            if t not in relation["tuples"]["positives"]: # if we add new tuple
                new_tuples_added = True
                relation["tuples"]["positives"].append(t)

    if new_tuples_added == False:
        break

pprint.pprint(relations)
