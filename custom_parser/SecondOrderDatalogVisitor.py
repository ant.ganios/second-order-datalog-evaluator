# Generated from SecondOrderDatalog.g4 by ANTLR 4.10.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SecondOrderDatalogParser import SecondOrderDatalogParser
else:
    from SecondOrderDatalogParser import SecondOrderDatalogParser

# This class defines a complete generic visitor for a parse tree produced by SecondOrderDatalogParser.

class SecondOrderDatalogVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by SecondOrderDatalogParser#term.
    def visitTerm(self, ctx:SecondOrderDatalogParser.TermContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#terms.
    def visitTerms(self, ctx:SecondOrderDatalogParser.TermsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#atom.
    def visitAtom(self, ctx:SecondOrderDatalogParser.AtomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#literal.
    def visitLiteral(self, ctx:SecondOrderDatalogParser.LiteralContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#literals.
    def visitLiterals(self, ctx:SecondOrderDatalogParser.LiteralsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#goal.
    def visitGoal(self, ctx:SecondOrderDatalogParser.GoalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#body.
    def visitBody(self, ctx:SecondOrderDatalogParser.BodyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#fact_terms.
    def visitFact_terms(self, ctx:SecondOrderDatalogParser.Fact_termsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#fact.
    def visitFact(self, ctx:SecondOrderDatalogParser.FactContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#r.
    def visitR(self, ctx:SecondOrderDatalogParser.RContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#rules.
    def visitRules(self, ctx:SecondOrderDatalogParser.RulesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SecondOrderDatalogParser#program.
    def visitProgram(self, ctx:SecondOrderDatalogParser.ProgramContext):
        return self.visitChildren(ctx)



del SecondOrderDatalogParser