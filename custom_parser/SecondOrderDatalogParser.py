# Generated from SecondOrderDatalog.g4 by ANTLR 4.10.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,9,100,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,1,0,1,0,1,1,1,1,1,
        1,5,1,30,8,1,10,1,12,1,33,9,1,1,2,1,2,1,2,1,2,1,2,5,2,40,8,2,10,
        2,12,2,43,9,2,1,3,1,3,1,3,3,3,48,8,3,1,4,1,4,1,4,5,4,53,8,4,10,4,
        12,4,56,9,4,1,5,1,5,1,6,1,6,1,7,1,7,1,7,5,7,65,8,7,10,7,12,7,68,
        9,7,1,8,1,8,1,8,1,8,1,8,5,8,75,8,8,10,8,12,8,78,9,8,1,9,1,9,1,9,
        1,9,1,9,1,9,1,9,1,9,3,9,88,8,9,1,10,5,10,91,8,10,10,10,12,10,94,
        9,10,1,10,1,10,1,11,1,11,1,11,0,0,12,0,2,4,6,8,10,12,14,16,18,20,
        22,0,1,1,0,7,8,95,0,24,1,0,0,0,2,26,1,0,0,0,4,34,1,0,0,0,6,47,1,
        0,0,0,8,49,1,0,0,0,10,57,1,0,0,0,12,59,1,0,0,0,14,61,1,0,0,0,16,
        69,1,0,0,0,18,87,1,0,0,0,20,92,1,0,0,0,22,97,1,0,0,0,24,25,7,0,0,
        0,25,1,1,0,0,0,26,31,3,0,0,0,27,28,5,1,0,0,28,30,3,0,0,0,29,27,1,
        0,0,0,30,33,1,0,0,0,31,29,1,0,0,0,31,32,1,0,0,0,32,3,1,0,0,0,33,
        31,1,0,0,0,34,41,3,0,0,0,35,36,5,2,0,0,36,37,3,2,1,0,37,38,5,3,0,
        0,38,40,1,0,0,0,39,35,1,0,0,0,40,43,1,0,0,0,41,39,1,0,0,0,41,42,
        1,0,0,0,42,5,1,0,0,0,43,41,1,0,0,0,44,48,3,4,2,0,45,46,5,4,0,0,46,
        48,3,4,2,0,47,44,1,0,0,0,47,45,1,0,0,0,48,7,1,0,0,0,49,54,3,6,3,
        0,50,51,5,1,0,0,51,53,3,6,3,0,52,50,1,0,0,0,53,56,1,0,0,0,54,52,
        1,0,0,0,54,55,1,0,0,0,55,9,1,0,0,0,56,54,1,0,0,0,57,58,3,4,2,0,58,
        11,1,0,0,0,59,60,3,8,4,0,60,13,1,0,0,0,61,66,5,8,0,0,62,63,5,1,0,
        0,63,65,5,8,0,0,64,62,1,0,0,0,65,68,1,0,0,0,66,64,1,0,0,0,66,67,
        1,0,0,0,67,15,1,0,0,0,68,66,1,0,0,0,69,76,5,8,0,0,70,71,5,2,0,0,
        71,72,3,14,7,0,72,73,5,3,0,0,73,75,1,0,0,0,74,70,1,0,0,0,75,78,1,
        0,0,0,76,74,1,0,0,0,76,77,1,0,0,0,77,17,1,0,0,0,78,76,1,0,0,0,79,
        80,3,10,5,0,80,81,5,5,0,0,81,82,3,12,6,0,82,83,5,6,0,0,83,88,1,0,
        0,0,84,85,3,16,8,0,85,86,5,6,0,0,86,88,1,0,0,0,87,79,1,0,0,0,87,
        84,1,0,0,0,88,19,1,0,0,0,89,91,3,18,9,0,90,89,1,0,0,0,91,94,1,0,
        0,0,92,90,1,0,0,0,92,93,1,0,0,0,93,95,1,0,0,0,94,92,1,0,0,0,95,96,
        5,0,0,1,96,21,1,0,0,0,97,98,3,20,10,0,98,23,1,0,0,0,8,31,41,47,54,
        66,76,87,92
    ]

class SecondOrderDatalogParser ( Parser ):

    grammarFileName = "SecondOrderDatalog.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "','", "'('", "')'", "'!'", "':-'", "'.'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "VARIABLE", 
                      "CONSTANT", "WS" ]

    RULE_term = 0
    RULE_terms = 1
    RULE_atom = 2
    RULE_literal = 3
    RULE_literals = 4
    RULE_goal = 5
    RULE_body = 6
    RULE_fact_terms = 7
    RULE_fact = 8
    RULE_r = 9
    RULE_rules = 10
    RULE_program = 11

    ruleNames =  [ "term", "terms", "atom", "literal", "literals", "goal", 
                   "body", "fact_terms", "fact", "r", "rules", "program" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    VARIABLE=7
    CONSTANT=8
    WS=9

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.10.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class TermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONSTANT(self):
            return self.getToken(SecondOrderDatalogParser.CONSTANT, 0)

        def VARIABLE(self):
            return self.getToken(SecondOrderDatalogParser.VARIABLE, 0)

        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm" ):
                return visitor.visitTerm(self)
            else:
                return visitor.visitChildren(self)




    def term(self):

        localctx = SecondOrderDatalogParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            _la = self._input.LA(1)
            if not(_la==SecondOrderDatalogParser.VARIABLE or _la==SecondOrderDatalogParser.CONSTANT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SecondOrderDatalogParser.TermContext)
            else:
                return self.getTypedRuleContext(SecondOrderDatalogParser.TermContext,i)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_terms

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerms" ):
                listener.enterTerms(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerms" ):
                listener.exitTerms(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerms" ):
                return visitor.visitTerms(self)
            else:
                return visitor.visitChildren(self)




    def terms(self):

        localctx = SecondOrderDatalogParser.TermsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_terms)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 26
            self.term()
            self.state = 31
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SecondOrderDatalogParser.T__0:
                self.state = 27
                self.match(SecondOrderDatalogParser.T__0)
                self.state = 28
                self.term()
                self.state = 33
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AtomContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.TermContext,0)


        def terms(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SecondOrderDatalogParser.TermsContext)
            else:
                return self.getTypedRuleContext(SecondOrderDatalogParser.TermsContext,i)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom" ):
                listener.enterAtom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom" ):
                listener.exitAtom(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtom" ):
                return visitor.visitAtom(self)
            else:
                return visitor.visitChildren(self)




    def atom(self):

        localctx = SecondOrderDatalogParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_atom)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 34
            self.term()
            self.state = 41
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SecondOrderDatalogParser.T__1:
                self.state = 35
                self.match(SecondOrderDatalogParser.T__1)
                self.state = 36
                self.terms()
                self.state = 37
                self.match(SecondOrderDatalogParser.T__2)
                self.state = 43
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LiteralContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.AtomContext,0)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLiteral" ):
                listener.enterLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLiteral" ):
                listener.exitLiteral(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLiteral" ):
                return visitor.visitLiteral(self)
            else:
                return visitor.visitChildren(self)




    def literal(self):

        localctx = SecondOrderDatalogParser.LiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_literal)
        try:
            self.state = 47
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SecondOrderDatalogParser.VARIABLE, SecondOrderDatalogParser.CONSTANT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 44
                self.atom()
                pass
            elif token in [SecondOrderDatalogParser.T__3]:
                self.enterOuterAlt(localctx, 2)
                self.state = 45
                self.match(SecondOrderDatalogParser.T__3)
                self.state = 46
                self.atom()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LiteralsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def literal(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SecondOrderDatalogParser.LiteralContext)
            else:
                return self.getTypedRuleContext(SecondOrderDatalogParser.LiteralContext,i)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_literals

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLiterals" ):
                listener.enterLiterals(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLiterals" ):
                listener.exitLiterals(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLiterals" ):
                return visitor.visitLiterals(self)
            else:
                return visitor.visitChildren(self)




    def literals(self):

        localctx = SecondOrderDatalogParser.LiteralsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_literals)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 49
            self.literal()
            self.state = 54
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SecondOrderDatalogParser.T__0:
                self.state = 50
                self.match(SecondOrderDatalogParser.T__0)
                self.state = 51
                self.literal()
                self.state = 56
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GoalContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.AtomContext,0)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_goal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGoal" ):
                listener.enterGoal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGoal" ):
                listener.exitGoal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGoal" ):
                return visitor.visitGoal(self)
            else:
                return visitor.visitChildren(self)




    def goal(self):

        localctx = SecondOrderDatalogParser.GoalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_goal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 57
            self.atom()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BodyContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def literals(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.LiteralsContext,0)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBody" ):
                listener.enterBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBody" ):
                listener.exitBody(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBody" ):
                return visitor.visitBody(self)
            else:
                return visitor.visitChildren(self)




    def body(self):

        localctx = SecondOrderDatalogParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_body)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 59
            self.literals()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Fact_termsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONSTANT(self, i:int=None):
            if i is None:
                return self.getTokens(SecondOrderDatalogParser.CONSTANT)
            else:
                return self.getToken(SecondOrderDatalogParser.CONSTANT, i)

        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_fact_terms

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFact_terms" ):
                listener.enterFact_terms(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFact_terms" ):
                listener.exitFact_terms(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFact_terms" ):
                return visitor.visitFact_terms(self)
            else:
                return visitor.visitChildren(self)




    def fact_terms(self):

        localctx = SecondOrderDatalogParser.Fact_termsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_fact_terms)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self.match(SecondOrderDatalogParser.CONSTANT)
            self.state = 66
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SecondOrderDatalogParser.T__0:
                self.state = 62
                self.match(SecondOrderDatalogParser.T__0)
                self.state = 63
                self.match(SecondOrderDatalogParser.CONSTANT)
                self.state = 68
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FactContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONSTANT(self):
            return self.getToken(SecondOrderDatalogParser.CONSTANT, 0)

        def fact_terms(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SecondOrderDatalogParser.Fact_termsContext)
            else:
                return self.getTypedRuleContext(SecondOrderDatalogParser.Fact_termsContext,i)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_fact

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFact" ):
                listener.enterFact(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFact" ):
                listener.exitFact(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFact" ):
                return visitor.visitFact(self)
            else:
                return visitor.visitChildren(self)




    def fact(self):

        localctx = SecondOrderDatalogParser.FactContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_fact)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(SecondOrderDatalogParser.CONSTANT)
            self.state = 76
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SecondOrderDatalogParser.T__1:
                self.state = 70
                self.match(SecondOrderDatalogParser.T__1)
                self.state = 71
                self.fact_terms()
                self.state = 72
                self.match(SecondOrderDatalogParser.T__2)
                self.state = 78
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def goal(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.GoalContext,0)


        def body(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.BodyContext,0)


        def fact(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.FactContext,0)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_r

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterR" ):
                listener.enterR(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitR" ):
                listener.exitR(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitR" ):
                return visitor.visitR(self)
            else:
                return visitor.visitChildren(self)




    def r(self):

        localctx = SecondOrderDatalogParser.RContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_r)
        try:
            self.state = 87
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 79
                self.goal()
                self.state = 80
                self.match(SecondOrderDatalogParser.T__4)
                self.state = 81
                self.body()
                self.state = 82
                self.match(SecondOrderDatalogParser.T__5)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 84
                self.fact()
                self.state = 85
                self.match(SecondOrderDatalogParser.T__5)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RulesContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(SecondOrderDatalogParser.EOF, 0)

        def r(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SecondOrderDatalogParser.RContext)
            else:
                return self.getTypedRuleContext(SecondOrderDatalogParser.RContext,i)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_rules

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRules" ):
                listener.enterRules(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRules" ):
                listener.exitRules(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRules" ):
                return visitor.visitRules(self)
            else:
                return visitor.visitChildren(self)




    def rules(self):

        localctx = SecondOrderDatalogParser.RulesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_rules)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 92
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SecondOrderDatalogParser.VARIABLE or _la==SecondOrderDatalogParser.CONSTANT:
                self.state = 89
                self.r()
                self.state = 94
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 95
            self.match(SecondOrderDatalogParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def rules(self):
            return self.getTypedRuleContext(SecondOrderDatalogParser.RulesContext,0)


        def getRuleIndex(self):
            return SecondOrderDatalogParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = SecondOrderDatalogParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_program)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 97
            self.rules()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





