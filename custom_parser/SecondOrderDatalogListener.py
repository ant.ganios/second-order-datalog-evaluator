# Generated from SecondOrderDatalog.g4 by ANTLR 4.10.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SecondOrderDatalogParser import SecondOrderDatalogParser
else:
    from SecondOrderDatalogParser import SecondOrderDatalogParser

# This class defines a complete listener for a parse tree produced by SecondOrderDatalogParser.
class SecondOrderDatalogListener(ParseTreeListener):

    # Enter a parse tree produced by SecondOrderDatalogParser#term.
    def enterTerm(self, ctx:SecondOrderDatalogParser.TermContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#term.
    def exitTerm(self, ctx:SecondOrderDatalogParser.TermContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#terms.
    def enterTerms(self, ctx:SecondOrderDatalogParser.TermsContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#terms.
    def exitTerms(self, ctx:SecondOrderDatalogParser.TermsContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#atom.
    def enterAtom(self, ctx:SecondOrderDatalogParser.AtomContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#atom.
    def exitAtom(self, ctx:SecondOrderDatalogParser.AtomContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#literal.
    def enterLiteral(self, ctx:SecondOrderDatalogParser.LiteralContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#literal.
    def exitLiteral(self, ctx:SecondOrderDatalogParser.LiteralContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#literals.
    def enterLiterals(self, ctx:SecondOrderDatalogParser.LiteralsContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#literals.
    def exitLiterals(self, ctx:SecondOrderDatalogParser.LiteralsContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#goal.
    def enterGoal(self, ctx:SecondOrderDatalogParser.GoalContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#goal.
    def exitGoal(self, ctx:SecondOrderDatalogParser.GoalContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#body.
    def enterBody(self, ctx:SecondOrderDatalogParser.BodyContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#body.
    def exitBody(self, ctx:SecondOrderDatalogParser.BodyContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#fact_terms.
    def enterFact_terms(self, ctx:SecondOrderDatalogParser.Fact_termsContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#fact_terms.
    def exitFact_terms(self, ctx:SecondOrderDatalogParser.Fact_termsContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#fact.
    def enterFact(self, ctx:SecondOrderDatalogParser.FactContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#fact.
    def exitFact(self, ctx:SecondOrderDatalogParser.FactContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#r.
    def enterR(self, ctx:SecondOrderDatalogParser.RContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#r.
    def exitR(self, ctx:SecondOrderDatalogParser.RContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#rules.
    def enterRules(self, ctx:SecondOrderDatalogParser.RulesContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#rules.
    def exitRules(self, ctx:SecondOrderDatalogParser.RulesContext):
        pass


    # Enter a parse tree produced by SecondOrderDatalogParser#program.
    def enterProgram(self, ctx:SecondOrderDatalogParser.ProgramContext):
        pass

    # Exit a parse tree produced by SecondOrderDatalogParser#program.
    def exitProgram(self, ctx:SecondOrderDatalogParser.ProgramContext):
        pass



del SecondOrderDatalogParser