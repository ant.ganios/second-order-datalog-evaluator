grammar SecondOrderDatalog;


term : CONSTANT | VARIABLE;
terms : term (',' term)*;

atom: term ('(' terms ')')*; 

literal: atom
    | '!' atom;
literals : literal (',' literal)*;


goal : atom;
body: literals;


fact_terms : CONSTANT (',' CONSTANT)*;
fact : CONSTANT ('(' fact_terms ')')*;

r : goal ':-' body'.' // using 'r' instead of rule because 'rule' is reserved as keyword'
  | fact'.';


rules : r* EOF;

program: rules;

VARIABLE: VARIABLE_START VARIABLE_REST*; // variables have an uppercase first letter

fragment VARIABLE_START : [A-Z] ;
fragment VARIABLE_REST : [a-zA-Z0-9] ;


CONSTANT : CONSTANT_START CONSTANT_REST* ;

fragment CONSTANT_START : [a-z] ;
fragment CONSTANT_REST : [a-zA-Z0-9] ;


WS : [ \t\r\n]+ -> skip ;
